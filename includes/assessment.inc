<?php
/*
* @file
* This include contains helper functions for the assessment module
*/

function assessment_query_nodes(&$nodetype) {

  $items = array();
  
  $result = db_query('
  SELECT n.nid, n.title
  FROM {node} n
  WHERE n.type = :type
  ORDER BY n.nid ASC',
  array(':type' => $nodetype));
  
  foreach ($result as $row) {
    $items[] = array('nid' => $row->nid, 'title' => $row->title);
  }

  return $items;
}

function assessment_get_dimensions(&$formname) {
  $items = array();
  
  $result = db_query('
  SELECT v.name
  FROM {variable} v
  WHERE v.name LIKE :assessment AND v.value = :related
  ORDER BY v.name ASC',
  array(':assessment' => db_like("assessment_dimension_") . '%' . db_like($formname), ':related' => 'i:1;'));
  
  foreach ($result as $row) {
    // filter the right subjects
    if (preg_match('/^assessment_dimension_(\d*)_'.$formname.'$/', $row->name, $matches)) {
      $items[] = $matches[1];
    }
  }
  return $items;
}

function assessment_get_subjects(&$dimensions) {

  $items = array();
  
  $result = db_query('
  SELECT s.nid
  FROM {node} s
  INNER JOIN {field_data_field_ref_dimension} d ON d.entity_id = s.nid
  WHERE s.type = :type AND d.field_ref_dimension_target_id IN (:dimension)
  ORDER BY s.title ASC',
  array(':type' => 'subject', ':dimension' => $dimensions));
  
  foreach ($result as $row) {
    $items[] = array('nid' => $row->nid);
  }
  
  return $items;
}

function assessment_get_questions(&$subjects, &$order, &$question_type) {
  // get questions related to an array of subjects and ordered by $order
 	$items = array();
 	$type = '';
	$query = db_select('node' , 'n')
	->fields('n', array('nid', 'title'))
	->fields('r', array('field_direction_value'))
	->fields('c', array('field_controls_other_question_target_id'))
	->fields('t', array('field_question_type_value'));
	
  $query->leftJoin('field_data_field_controls_other_question', 'c', 'c.entity_id = n.nid');
	$query->innerJoin('field_data_field_direction', 'r', 'r.entity_id = n.nid');
  $query->leftJoin('field_data_field_ref_subject', 's' ,'s.entity_id = n.nid');
  $query->leftJoin('field_data_field_question_type', 't', 't.entity_id = n.nid');
	$query->condition('n.type', 'question', '=');
	$query->condition('s.field_ref_subject_target_id', $subjects, 'IN');
	
	if (!empty($question_type)) {
	  $query->condition('t.field_question_type_value', $question_type, '=');
	}
	if ($order != 'natural') {
    if ($order != 'orderRandom') {
      $query->orderBy($order, 'ASC');
    } else {
      $query->orderRandom();
    }
  }
  $result = $query->execute();
		
	foreach($result as $row) {
		$items[] = array(
		  $row->nid,
		  $row->title,
		  $row->field_direction_value,
		  $row->field_controls_other_question_target_id,
		  $row->field_question_type_value
		  );
	}
  	return $items;
}

function assessment_get_assessments(&$assessments_uid) {

  // always include registered user role
  $role_ids[] = 2;

  // get other roles for user
  $result = db_query('
  SELECT ur.rid
  FROM {users_roles} ur
  WHERE ur.uid = :uid AND ur.rid <> :roles',
  array(':uid' => $assessments_uid, ':roles' => 2));
  
  // because we use [] other roles get appended to the existing role 2
  foreach ($result as $row) {
    $role_ids[] = intval($row->rid);
  }
  
  // get the "create [type]" permissions
  $assessments = array();
  $result = db_query('
  SELECT r.permission
  FROM {role_permission} r
  WHERE r.rid IN (:roles) AND r.module IN (:module) AND r.permission LIKE :permission',
  array(':roles' => $role_ids, ':module' => 'node', ':permission' => db_like("create ") . '%'));

  foreach ($result as $row) {
    //check if the create content type permission is for an assessment
    if (variable_get('assessment_button_' . substr(substr($row->permission, 7), 0, -8)) == 1) {
        $assessments[] = substr(substr($row->permission, 7), 0, -8);
      }
    }
  return $assessments;

}

function assessment_get_assessments_editable(&$assessments_uid) {

  // always include registered user role
  $role_ids[] = 2;

  // get other roles for user
  $result = db_query('
  SELECT ur.rid
  FROM {users_roles} ur
  WHERE ur.uid = :uid AND ur.rid <> :roles',
  array(':uid' => $assessments_uid, ':roles' => 2));
  
  // because we use [] other roles get appended to the existing role 2
  foreach ($result as $row) {
    $role_ids[] = intval($row->rid);
  }
  
  // get the "edit own [type]" permissions
  $assessments_editable = array();
  $result = db_query('
  SELECT r.permission
  FROM {role_permission} r
  WHERE r.rid IN (:roles) AND r.module IN (:module) AND r.permission LIKE :permission',
  array(':roles' => $role_ids, ':module' => 'node', ':permission' => db_like("edit own ") . '%'));

  foreach ($result as $row) {
    //check if the edit own content type permission is for an assessment
    if (variable_get('assessment_button_' . substr(substr($row->permission, 9), 0, -8)) == 1) {
        $assessments_editable[] = substr(substr($row->permission, 9), 0, -8);
      }
    }
  return $assessments_editable;

}

function assessment_get_nid_from_subject(&$assessment_subject) {

  $query = db_query('
    SELECT subj.entity_id
    FROM {field_data_field_subject_system} subj
    WHERE subj.field_subject_system_value = :assessment_subject',
    array(':assessment_subject' => $assessment_subject));
    
  $result = $query->execute;
  
  return $result;
}

function assessment_get_existing(&$assessment_type, &$assessment_uid) {
  
  $result = db_query('
  SELECT n.nid
  FROM {node} n
  WHERE n.type = :type AND n.uid = :uid
  ORDER BY n.nid DESC
  LIMIT 1',
  array(':type' => $assessment_type, ':uid' => $assessment_uid))->fetchField();

  return $result;

}

function assessment_get_questions_answers(&$formname) {

  global $user;
  $items = array(); 

  $query = db_select('node' , 'n')
  ->fields('n', array('nid', 'title'))
//   ->fields('used', array('entity_id'))
  ->orderby('nid', 'DESC');
//   $query->rightjoin('field_data_field_subject_system', 'used', 'used.field_is_used_in_value = :type', array(':type' => $formname));
//   $query->leftjoin('variables', 'v', 'v.name ');
//   $query->range(0,1);
	$query->condition('n.type', 'subject', '=');
	$query->condition('n.title', $subjects, 'IN');
	
//	$query->condition('n.uid', $user->uid, '=');	
  
  $result = $query->execute();
	foreach($result as $row) {
		$items[] = array(
		  $row->nid,
		  $row->title
// 		  $row->entity_id
		  );
	}
  	return $items;
}

function assessment_get_answeroptions(&$answertype) {

  $result = db_query('
  SELECT fc.data
  FROM {field_config} fc
  INNER JOIN {field_config_instance} fci ON fci.field_id = fc.id
  WHERE fci.bundle = :answertype AND fci.field_name NOT IN (:question)',
  array(':answertype' => $answertype, ':question' => 'field_referenced_question'))->fetchField();
  
  return $result;
}
