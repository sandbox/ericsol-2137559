<?php
/**
 * @file
 * assessment_types.features.inc
 */

/**
 * Implements hook_node_info().
 */
function assessment_types_node_info() {
  $items = array(
    'assessment_structures' => array(
      'name' => t('assessment structures'),
      'base' => 'node_content',
      'description' => t('The assessment structures contain the underlying validity fields and field collections for different answer types.
ONLY EDIT THIS IF YOU KNOW WHAT YOU ARE DOING!'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'dimension' => array(
      'name' => t('dimension'),
      'base' => 'node_content',
      'description' => t('A dimension contains a number of subjects. It is - or should be - a rather generic description of a "quality". Big 5 and Great 8 are examples of dimensions. They may have extra properties that can not be set for individual subjects.'),
      'has_title' => '1',
      'title_label' => t('Dimension'),
      'help' => '',
    ),
    'question' => array(
      'name' => t('question'),
      'base' => 'node_content',
      'description' => t('A question is the singular part of a questionnaire. Questions are referenced from field collections for different answer types. Not all fields are necessary, e.g. labels and values for multiple choice fields only need to be filled in if this question is rendered as part of the field_answers_mc field collection.'),
      'has_title' => '1',
      'title_label' => t('question'),
      'help' => '',
    ),
    'subject' => array(
      'name' => t('subject'),
      'base' => 'node_content',
      'description' => t('Stores subject data. A subject can be part of one or more assessments.'),
      'has_title' => '1',
      'title_label' => t('title'),
      'help' => '',
    ),
  );
  return $items;
}
